import UIKit

//If you copy a Struct, it will be the same.

//If you copy a Class, it will be different.

struct Singer {
    var name = "Taylor Swift"
}

var singer = Singer()
print(singer.name)

var singerCopy = singer
singerCopy.name = "Justin Bieber"

print(singer.name)
