import UIKit

//This declares this Class as FINAL, nothing can change this behaviour.

final class Dog {
    var name = String
    var breed = String
    
    init(name: String, breed: String) {
        self.name = name
        self.breed = breed
    }
}
