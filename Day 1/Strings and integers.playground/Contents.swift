import UIKit

//String
var greeting = "Hello, playground"

//Integer
var age = 38

//Underscores make numbers easier to read
var population = 8_000_000

