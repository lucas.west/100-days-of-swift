import UIKit

//These are empty collections. You can add entries, such as //Teams["Paul"] = "Red"

var teams = [String: String]()
teams["Paul"] = "Red"

var results = [Int]()

var words = Set<String>()
var numbers = Set<Int>()

var scores = Dictionary<String, Int>()
var results = Array<Int>()

