import UIKit

//enums are a wat of defining groups of related values in a way that makes them easier to use. In this case, we have assigned //result4 to result.failure and can be changed each time.

let result = "Failure"
let result2 = "Fail"
let result3 = "Failed"

enum Result {
    case success
    case failure
}

let result4 = Result.failure
