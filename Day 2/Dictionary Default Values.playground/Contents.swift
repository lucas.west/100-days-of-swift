import UIKit

let favouriteIceCream = [
    "Paul": "Chocolate",
    "Sophie": "Vanilla"
]

favouriteIceCream["Paul"]
favouriteIceCream["Charlotte", default: "Unknown"]
