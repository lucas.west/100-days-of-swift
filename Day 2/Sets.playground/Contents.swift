import UIKit

//All items must be unique in a SET, will not repeat.

let colors = Set(["red", "green", "blue"])
let colors2 = Set(["red", "green", "blue", "red", "blue"])

