import UIKit

//If you needed a specific fixed range of values, such as a name, use a tuple.

let addresss = (house: 555, street: "Taylor Swift Avenue", city: "Nashville")

//If you need a selection of values which are unique, use a set.

let set = Set(["ardvark", "astronaut", "azalea"])

//If you need a selection of values with duplicates, use an array.
let pythons = ["Eric", "Graham", "John", "Michael", "Terry", "Terry"]
