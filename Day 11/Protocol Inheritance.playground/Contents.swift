import UIKit

//You can inherit from multiple Protocols at the same time.

protocol Payable {
    func calculateWages() -> Int
}

protocol NeedsTraining {
    func study()
}

protocol HasVacation {
    func takeVacation(days: Int)
}

protocol Employee: Payable, NeedsTraining, HasVacation { }

//Now you can write all in the curly braces, rather than all 3 seperately.
