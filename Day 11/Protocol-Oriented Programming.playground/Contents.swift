import UIKit

protocol Identifiable {
    var id: String { get set }
    func identify()
}

extension Identifiable {
    func identity() {
        print("My Id is \(id).")
    }
}

struct User: Identifiable {
    var id: String
}

let twostraws = User(id: "twostraws")
twostraws.identify()
