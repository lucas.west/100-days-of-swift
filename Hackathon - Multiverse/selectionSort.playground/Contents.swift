import Foundation
import CoreFoundation

class ParkBenchTimer {
    let startTime: CFAbsoluteTime
    var endTime: CFAbsoluteTime?

    init() {
        startTime = CFAbsoluteTimeGetCurrent()
    }

    func stop() -> CFAbsoluteTime {
        endTime = CFAbsoluteTimeGetCurrent()

        return duration!
    }

    var duration: CFAbsoluteTime? {
        if let endTime = endTime {
            return endTime - startTime
        } else {
            return nil
        }
    }
}

public extension Array where Element == Int {
    static func generateRandomNumbers(size: Int) -> [Int] {
        guard size > 0 else {
            return [Int]()
    }
    
        let result = Array(repeating: 0, count: size)
        return result.map{ _ in Int.random(in: 0..<size) }
}
    
    static func generateUniqueRandomNumbers(size: Int) -> [Int] {
        guard size > 0 else {
            return [Int]()
    }
    
        return Array(0..<size).shuffled()
    }
}

var RandomInts = Array.generateRandomNumbers(size: 50)

var uniqueRandomInts = Array.generateUniqueRandomNumbers(size: 50)

let timer = ParkBenchTimer()

func selectionSort(uniqueRandomInts: inout [Int]) {
    // 1. loop from the beginning of the array to the second to last item
    for currentIndex in 0..<(uniqueRandomInts.count - 1) {
        // 2. save a copy of the currentIndex
        var minIndex = currentIndex;
        // 3. loop through all indexes that proceed the currentIndex
        for i in (currentIndex + 1)..<uniqueRandomInts.count {
            // 4.  if the value of the index of the current loop is less
            //          than the value of the item at minIndex, update minIndex
            //          with the new lowest value index */
            if (uniqueRandomInts[i] < uniqueRandomInts[minIndex]) {
                // update minIndex with the new lowest value index
                minIndex = i;
            }
        }
        // 5. if minIndex has been updated, swap the values at minIndex and currentIndex
        if (minIndex != currentIndex) {
            let temp = uniqueRandomInts[currentIndex];
            uniqueRandomInts[currentIndex] = uniqueRandomInts[minIndex];
            uniqueRandomInts[minIndex] = temp;
        }
    }
}

selectionSort(uniqueRandomInts: &uniqueRandomInts)
print(uniqueRandomInts)

print("The task took \(timer.stop()) seconds.")
