import Foundation
import CoreFoundation

class ParkBenchTimer {
    let startTime: CFAbsoluteTime
    var endTime: CFAbsoluteTime?

    init() {
        startTime = CFAbsoluteTimeGetCurrent()
    }

    func stop() -> CFAbsoluteTime {
        endTime = CFAbsoluteTimeGetCurrent()

        return duration!
    }

    var duration: CFAbsoluteTime? {
        if let endTime = endTime {
            return endTime - startTime
        } else {
            return nil
        }
    }
}

public extension Array where Element == Int {
    static func generateRandomNumbers(size: Int) -> [Int] {
        guard size > 0 else {
            return [Int]()
    }
    
        let result = Array(repeating: 0, count: size)
        return result.map{ _ in Int.random(in: 0..<size) }
}
    
    static func generateUniqueRandomNumbers(size: Int) -> [Int] {
        guard size > 0 else {
            return [Int]()
    }
    
        return Array(0..<size).shuffled()
    }
}


var RandomInts = Array.generateRandomNumbers(size: 50)

var uniqueRandomInts = Array.generateUniqueRandomNumbers(size: 50)

let timer = ParkBenchTimer()

func insertionSort(_ array: [Int]) -> [Int] {
    var sortedArray = array
    // 1. make a copy of the Array
    for index in 1..<sortedArray.count {
        // 2. the outer loop looks at each of the elements in the array in turn; this is what picks the top-most number from the pile. The variable currentIndex is the index of where the sorted portion ends and the pile begins
        var currentIndex = index
        while currentIndex > 0 && sortedArray[currentIndex] < sortedArray[currentIndex - 1] {
            // 3. The inner loop looks at the element at position currentIndex. This is the number at the top of the pile, and it may be smaller than any of the previous elements. The inner loop steps backwards through the sorted array; every time it finds a previous number that is larger, it swaps them. When the inner loop completes, the beginning of the array is sorted again, and the sorted portion has grown by one element.
            sortedArray.swapAt(currentIndex - 1, currentIndex)
            currentIndex -= 1
        }
    }
    return sortedArray
    // 4. call the function
}

print(insertionSort(uniqueRandomInts))

print("The task took \(timer.stop()) seconds.")
