import Foundation
import CoreFoundation

class ParkBenchTimer {
    let startTime: CFAbsoluteTime
    var endTime: CFAbsoluteTime?

    init() {
        startTime = CFAbsoluteTimeGetCurrent()
    }

    func stop() -> CFAbsoluteTime {
        endTime = CFAbsoluteTimeGetCurrent()

        return duration!
    }

    var duration: CFAbsoluteTime? {
        if let endTime = endTime {
            return endTime - startTime
        } else {
            return nil
        }
    }
}

public extension Array where Element == Int {
    static func generateRandomNumbers(size: Int) -> [Int] {
        guard size > 0 else {
            return [Int]()
    }
    
        let result = Array(repeating: 0, count: size)
        return result.map{ _ in Int.random(in: 0..<size) }
}
    
    static func generateUniqueRandomNumbers(size: Int) -> [Int] {
        guard size > 0 else {
            return [Int]()
    }
    
        return Array(0..<size).shuffled()
    }
}

var RandomInts = Array.generateRandomNumbers(size: 50)

var uniqueRandomInts = Array.generateUniqueRandomNumbers(size: 50)

let timer = ParkBenchTimer()

func bubbleSort(uniqueRandomInts: inout [Int]) {
    var n = uniqueRandomInts.count
    while (n > 0) {
        var lastModifiedIndex = 0
        for currentIndex in 1..<n {
            // 1. if the item at the previous index is greater than the item at the `currentIndex`, swap them
            if uniqueRandomInts[currentIndex - 1] > uniqueRandomInts[currentIndex] {
                // 2. swap
                let temp = uniqueRandomInts[currentIndex - 1]
                uniqueRandomInts[currentIndex - 1] = uniqueRandomInts[currentIndex]
                uniqueRandomInts[currentIndex] = temp
                // 3. save the index that was modified
                lastModifiedIndex = currentIndex
            }
        }
        // 4. save the last modified index so we know not to iterate past it since all proceeding values are sorted
        n = lastModifiedIndex
    }
}

bubbleSort(uniqueRandomInts: &uniqueRandomInts)
print(uniqueRandomInts)

print("The task took \(timer.stop()) seconds.")
