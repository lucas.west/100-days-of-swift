import UIKit

class Animal { }
class Fish: Animal { }

class Dog: Animal {
    func makeNoise() {
        print("Woof!")
    }
}

let pets = [Fish(), Dog(), Fish(), Dog()]

for pets in pets {
    if let dog = pets as? Dog {
        dog.makeNoise()
    }
}
