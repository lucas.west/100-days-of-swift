import UIKit

func travel(action: (String) -> String) {
    print("I'm getting ready to go.")
    let description = action("London")
    print(description)
    print("I have arrived!")
}

//Swift knows the parameters for this closure, so we can remove :String).

//It also knows a closure must return a string, so we can remove this too

//As a closure only has one line of code that must be the one that returns the value, so we can remove Return too.

//Swift has a shorthand syntax that let's me go shorter, rather than place in we can let Swift provode automatic names for the closures parameters. These are named with a dollar sign and a number counting from 0.

//Before
travel { (place:String) -> String in
    return "I'm going to \(place) in my car"
}

//After
travel {
    "I'm going to \($0) in my car"
}
