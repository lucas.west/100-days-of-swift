import UIKit

func travel(action: () -> Void) {
    print("I'm getting ready to go.")
    action()
    print("I have arrived!")
}

travel {
    print("I'm driving in my car")
}
