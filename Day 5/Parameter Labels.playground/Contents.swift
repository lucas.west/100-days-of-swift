import UIKit

func square(number: Int) -> Int {
    return number * number
}

let result = square(number: 8)

func sayhello(to name: String) {
    print("Hello, \(name)!")
}
